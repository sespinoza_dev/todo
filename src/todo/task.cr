require "redis"
require "colorize"
require "./status"

module Todo
  class Task
    
    def self.create(options, arguments)
      redis = Redis.new
      redis.set("id", 0) if !redis.get("id")
      redis.incr("id")
      id = redis.get("id")
      hash = Hash{"task" => arguments[0], "timestamp" => Time.now.to_s}
      redis.hmset("todo:#{id}:#{options.keyword}:unchecked", hash)
      
      status = Status.new("unchecked")
      keyword = options.keyword != "default" ? "#{options.keyword}\t".colorize(:light_green).to_s : ""
      puts "#{status.mark} [#{id}] #{keyword} #{arguments[0]}"
    end

    # todo: ls --markdown
    def self.ls(options, arguments)
      redis = Redis.new
      keyword = options.keyword

      keys = redis.keys("todo:*:#{keyword}:now")
      list_task(keys, redis)

      keys = redis.keys("todo:*:#{keyword}:unchecked")
      list_task(keys, redis)

      keys = redis.keys("todo:*:#{keyword}:checked")
      list_task(keys, redis)

      keys = redis.keys("todo:*:#{keyword}:cancelled")
      list_task(keys, redis)
    end

    def self.show(options, arguments)
      puts "show #{arguments[0]}"
    end

    def self.check(options, arguments)
      begin
        redis = Redis.new
        key = redis.keys("todo:#{arguments[0]}:*:*")[0]
        task = redis.hget(key, "task")
        splited_key = key.to_s.split(":")
        id = splited_key[1]
        keyword = splited_key[2]
        current_status = splited_key[3]

        redis.hset("todo:#{id}:#{keyword}:#{current_status}", "timestamp", Time.now.to_s)
        redis.rename("todo:#{arguments[0]}:#{keyword}:#{current_status}", "todo:#{arguments[0]}:#{keyword}:checked")

        status = Status.new("checked")
        puts "#{status.mark} [#{id}] #{task} #{keyword.colorize(:light_green).to_s}"
      rescue exception
        puts exception
      end
    end

    def self.uncheck(options, arguments)
      begin
        redis = Redis.new
        key = redis.keys("todo:#{arguments[0]}:*:*")[0]
        task = redis.hget(key, "task")
        splited_key = key.to_s.split(":")
        id = splited_key[1]
        keyword = splited_key[2]
        current_status = splited_key[3]

        redis.hset("todo:#{id}:#{keyword}:#{current_status}", "timestamp", Time.now.to_s)
        redis.rename("todo:#{arguments[0]}:#{keyword}:#{current_status}", "todo:#{arguments[0]}:#{keyword}:unchecked")

        status = Status.new("unchecked")
        puts "#{status.mark} [#{id}] #{task} #{keyword.colorize(:light_green).to_s}"
      rescue exception
        puts exception
      end
    end

    def self.now(options, arguments)
      begin
        redis = Redis.new
        key = redis.keys("todo:#{arguments[0]}:*:*")[0]
        task = redis.hget(key, "task")
        splited_key = key.to_s.split(":")
        id = splited_key[1]
        keyword = splited_key[2]
        current_status = splited_key[3]

        redis.hset("todo:#{id}:#{keyword}:#{current_status}", "timestamp", Time.now.to_s)
        redis.rename("todo:#{arguments[0]}:#{keyword}:#{current_status}", "todo:#{arguments[0]}:#{keyword}:now")

        status = Status.new("now")
        puts "#{status.mark} [#{id}] #{task} #{keyword.colorize(:light_green).to_s}"
      rescue exception
        puts exception
      end
    end

    def self.cancel(options, arguments)
      begin
        redis = Redis.new
        key = redis.keys("todo:#{arguments[0]}:*:*")[0]
        task = redis.hget(key, "task")
        splited_key = key.to_s.split(":")
        id = splited_key[1]
        keyword = splited_key[2]
        current_status = splited_key[3]

        redis.hset("todo:#{id}:#{keyword}:#{current_status}", "timestamp", Time.now.to_s)
        redis.rename("todo:#{arguments[0]}:#{keyword}:#{current_status}", "todo:#{arguments[0]}:#{keyword}:cancelled")
        
        status = Status.new("cancelled")
        puts "#{status.mark} [#{id}] #{task} #{keyword.colorize(:light_green).to_s}"
      rescue exception
        puts exception
      end
    end

    def self.update(options, arguments)
      puts "update #{arguments[0]}"
    end

    def self.del(options, arguments)
      begin
        redis = Redis.new
        key = redis.keys("todo:#{arguments[0]}:*:*")[0]
        task = redis.hget(key, "task")
        splited_key = key.to_s.split(":")
        id = splited_key[1]
        keyword = splited_key[2]
        current_status = splited_key[3]

        if options.hard
          redis.del("todo:#{arguments[0]}:#{keyword}:#{current_status}")
        else
          redis.hset("todo:#{id}:#{keyword}:#{current_status}", "timestamp", Time.now.to_s)
          redis.rename("todo:#{arguments[0]}:#{keyword}:#{current_status}", "todo:#{arguments[0]}:#{keyword}:deleted")
        end

        status = Status.new("deleted")
        puts "#{status.mark} [#{id}] #{task} #{keyword.colorize(:light_green).to_s}"
        puts "#{"warning".colorize(:yellow).to_s}: The task is still in redis, use --hard if you want to totally delete it." if !options.hard
      rescue exception
        puts exception
      end
    end

    def self.list_task(keys, redis)
      keys.each do |key|
        task = redis.hmget(key, "task", "timestamp")
        status = Status.new(key.to_s.split(":")[3])
        keyword = key.to_s.split(":")[2] != "default" ? key.to_s.split(":")[2].colorize(:light_green).to_s : ""
        id = key.to_s.split(":")[1]
        puts "#{status.mark} [#{id}] #{task[0]} #{keyword}"
      end
    end

    def self.bin(options, arguments)
      begin
        redis = Redis.new
        keys = redis.keys("todo:*:*:deleted")
        list_task(keys, redis)

        if options.drop
          keys.each do |key|
            redis.del(key)
          end
        end
        puts "#{"🗑".colorize(:white).to_s} well done you clean up your bin!" if options.drop
      rescue exception
        puts exception
      end
    end

  end
end