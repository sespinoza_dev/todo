require "json"

module Todo
  class Status

    def initialize(@text : String)
      case @text
      when "checked"
        @mark = "✔".colorize(:light_green).to_s
      when "unchecked"
        @mark = "-".colorize(:yellow).to_s
      when "deleted"
        @mark = "✝".colorize(:white).to_s
      when "now"
        @mark = "✱".colorize(:yellow).to_s
      else "cancelled"
        @mark = "✘".colorize(:red).to_s
      end
    end

    JSON.mapping(
      mark:     String,
      text:     String,
    )
  end
end