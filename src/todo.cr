require "./todo/*"
require "clim"

module Todo
  class TodoCli < Clim

    # version
    main_command do
      desc "Todo command line interface tool."
      usage "todo [options] [arguments] ..."
      version Todo::VERSION, short: "-v"
      option "--markdown", type: Bool, desc: "display as markdown format"
      option "-k <keyword>", "--keyword=<keyword>", type: String, desc: "list by keyword", default: "*"
      run do |options, arguments|
        Task.ls(options, arguments)
      end

      # new
      sub_command "new" do
        desc "create new todo"
        option "-k <keyword>", "--keyword=<keyword>", type: String, desc: "add a keyword to your task", default: "default"
        usage "todo new <task description> [-k <keyword> | --keyword=<keyword>]"
        run do |options, arguments|
          Task.create(options, arguments)
        end
      end

      # list
      sub_command "ls" do
        desc "list task"
        usage "todo ls [--markdown] [-k <key word> | --key=<key word>]"
        option "--markdown", type: Bool, desc: "display as markdown format"
        option "-k <keyword>", "--keyword=<keyword>", type: String, desc: "list by keyword", default: "*"
        run do |options, arguments|
          Task.ls(options, arguments)
        end
      end

      # show
      sub_command "show" do
        desc "show a specific task"
        usage "todo show <task id>"
        run do |options, arguments|
          Task.show(options, arguments)
        end
      end

      # check
      sub_command "check" do
        desc "check a specific task"
        usage "todo check <task id>"
        run do |options, arguments|
          Task.check(options, arguments)
        end
      end

      # uncheck
      sub_command "uncheck" do
        desc "uncheck a specific task"
        usage "todo uncheck <task id>"
        run do |options, arguments|
          Task.uncheck(options, arguments)
        end
      end

      # now
      sub_command "now" do
        desc "mark task as current"
        usage "todo now <task id>"
        run do |options, arguments|
          Task.now(options, arguments)
        end
      end

      # cancel
      sub_command "cancel" do
        desc "cancel a specific task"
        usage "todo cancel <task id>"
        run do |options, arguments|
          Task.cancel(options, arguments)
        end
      end

      # update
      sub_command "update" do
        desc "update a specific task"
        usage "todo update <task id> <argument>"
        run do |options, arguments|
          Task.update(options, arguments)
        end
      end

      # delete
      sub_command "del" do
        desc "delete a specific task"
        usage "todo del <task id> [--hard]"
        option "--hard", type: Bool, desc: "hard delete the task from redis", default: false
        run do |options, arguments|
          Task.del(options, arguments)
        end
      end

      # bin
      sub_command "bin" do
        desc "list and drop bin"
        usage "todo bin [--drop]"
        option "--drop", type: Bool, desc: "hard delete all the task from redis", default: false
        run do |options, arguments|
          Task.bin(options, arguments)
        end
      end

    end
  end

  Todo::TodoCli.start(ARGV)
end