# todo

Todo CLI app for every programming day!

## About

The todo app is a crystal command line micro app 
that is fast and keeps you focus. 

### Features

- Quickly list, check, change the status of task through simple commands.
- Keyword for better organization.
- Regex search.
- Flexible: Import and export from markdown files.
- Safe: Redis keep all the information.
- Light: 1.2M (current version 0.1.5)

## Installation

Install the denpedencies and build the binary.

```console
shards install
shards build
```

Add it to the path so you can use it as a command.

```console
mkdir -p ~/.local/bin/
cp bin/todo ~/.local/bin/todo
```

These previous commands are in a `deploy.sh` shell script that you can run as:

```console
bash deploy.sh
```

**Notice**: You need to have redis server running, if you haven't
installed redis yet, I recommend 
[this documentation](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-redis-on-ubuntu-16-04)

## Usage

First use the help method to display all the commands.

```console
$todo --help
```

Example.

```console
$todo --help
# display help

$todo new "conquer her heart"
new: [1] "conquer her heart"

$todo new "with chocolates!"
new: [2] "with chocolates!"

$todo check 1
✔ [1] conquer her heart
- [2] with chocolates!

$todo ls
✔ [1] conquer her heart
- [2] with chocolates!

$todo check 2
✔ [1] conquer her heart
✔ [2] with chocolates!

$todo del 2
✝ [2] with chocolates!
```

## Development

todo

- [x] CLI template response.
- [ ] CRUD todo.
  - [x] create.
  - [x] list.
  - [ ] read.
  - [ ] update.
    - [ ] text.
    - [x] status checked.
    - [x] status unchecked.
    - [x] status cancelled.
    - [x] status deleted (soft delete).
  - [x] delete (hard delete).
- [x] Order by pending, completed, cancelled.
- [x] List by keyword.
- [x] List deleted.
- [x] drop bind (hard delete tasks marked as deleted).
- [x] Mark taks as now (current).
- [ ] Export to .md file extension.
- [ ] Export to .pdf file extension.
- [ ] Import from .md file.
- [ ] Search todos with regex.

possible wanted for the future

- [ ] trello integration.

## Contributing

1. Clone the project (`git clone git@gitlab.com:sespinoz/todo.git && cd todo`)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am "Add some feature"`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request.

## Contributors

- [@sespinoz](https://gitlab.com/sespinoz) - creator and maintainer

## Acknowledgements

- Special thanks to [Daniel Salazar](https://github.com/disalazarg/) who encourage me to build apps in Crystal.
- Special thanks to [icons8](https://icons8.com/icons/set/check) for the icon for the project.